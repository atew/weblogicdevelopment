# Class: cesaweblogic
#
# This module manages cesaweblogic
#
# Parameters: none
#
# Actions:
#
# Requires: see Modulefile
#
# Sample Usage:
#
class cesaweblogic {

}

class weblogic($workdir =undef,$base_dir="/opt/oracle",$ora_home="/opt/oracle/Middleware",
  $wls_home="/opt/oracle/Middleware/wlserver_10.3",$coherence_home="/opt/oracle/Middleware/coherence_3.7"
) {
  
  if $workdir == undef {
    fail("Workdir must be set")
  }
#prepare directory folder

  file {[$base_dir,$ora_home]:
    ensure => directory,
    owner => oracle,
  }
  
 $silentFile = "${workdir}/wls-silent-xml.xml" 
#prepare silent file  
  file {$silentFile:
    content => template('cesaweblogic/wls-silent-xml.erb')    
  }
  
  $javaHome="/usr/java/jdk1.6.0_60"
#install weblogic server
  exec {'weblogic server':
    command =>"${javaHome}/bin/java -jar /opt/stage/Weblogic/wls1036_generic.jar -mode=silent -silent_xml=${silentFile}",

  } 
}