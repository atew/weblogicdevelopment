file {'/opt/stage':
  ensure => directory,
  before => Mount['/opt/stage']
}
mount { '/opt/stage':
  fstype => "nfs",
  atboot => true,
  device => "10.34.28.5:/opt/stage",
  ensure => mounted,
  options => "defaults",
  before => [Package['jdk-1.6.0_60-fcs']],
  
}
  
package { 'jdk-1.6.0_60-fcs' :
   ensure => present,
   require => Mount['/opt/stage'],
   source => "/opt/stage/Java/jdk-6u60-linux-amd64.rpm",
   allow_virtual => false,
   provider => 'rpm',
}

group {'dba':
  ensure => present,  
}

user {'oracle':
  ensure => present,
  groups => ["dba"]   
  
}
